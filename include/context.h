#ifndef CONTEXT_H

#define CONTEXT_H 
#include "list.h"

#define STACK_SIZE 4096

typedef void (*PFUNC)(void *);

enum ctx_state_t {NEW, ACTIVE, TERMINATED, FREE};

struct ctx_s 
{
    void *esp;
    void *ebp;
#define CTX_MAGIC 0xDEADBEEF
    int ctx_magic;

    PFUNC pfunc;
    void *arg;
    enum ctx_state_t state;
    struct list_head mycontext;

    int stack[STACK_SIZE];
};
struct ctx_s ctx_ping; 
struct ctx_s ctx_pong; 
struct ctx_s *current_ctx;
struct ctx_s free_ctx[16];

void irq_enable();
void irq_disable();
void init_ctx(struct ctx_s *ctx, PFUNC f, void *args);
int create_ctx(PFUNC f, void *args);
void yield();

#endif
