#include "minilib.h"

#define get_esp_ebp(x,y)                            \
    asm("movl %%esp, %0" "\n\t" "movl %%ebp, %1"    \
        : "=r"(x), "=r"(y))

#define put_esp_ebp(x,y)                            \
    asm("movl %%esp, %0" "\n\t" "movl %%ebp, %1"    \
        :                                           \
        : "r"(x), "r"(y))

#define get_esp(x)                                  \
    asm("movl %%esp, %0"                            \
        : "=r" (x))

#define get_ebp(x)                                  \
    asm("movl %%esp, %0"                            \
        : "=r"(x))

void print_esp_ebp(char *s, int x, int y)
{
    puts(s);
    puts("ESP = ");
    puthex(x);
}

struct ctx_s {
    int esp;
    int ebp;
#define CTX_MAGIC 0xDEADBEEF
    unsigned ctx_magic;
};

typedef int (func_t)(int);

int try(struct ctx_s *pctx, func_t *f, int arg)
{
    int r;

    pctx->ctx_magic = CTX_MAGIC;
    //save context into pctx
    get_esp_ebp(pctx->esp, pctx->ebp);
    //call f with arg
    r = f(arg);
    //not executed when trown
    puts("Pas de throw\n");
    // returns f value
    return r;
}

static int throw_r;

int throw(struct ctx_s *pctx, int r)
{
    assert(pctx->ctx_magic == CTX_MAGIC);
    throw_r = r;
    // get ctx
    put_esp_ebp(pctx->esp, pctx->ebp);
    

    //should be r
    return throw_r;
}

struct ctx_s global_ctx;

int myfunction(int x)
{
    int a =x, b = x+1;
    int myesp, myebp;

    get_esp_ebp(myesp, myebp);
    print_esp_ebp("myfun: ", myesp, myebp);

    if (x == 10)
        throw(&global_ctx, 0);
    else {
        puts("Tout va bien x = ");
        putud(x);
        puts("\n");
    }

    return a++;
}

void app_main()
{
    int myesp, myebp;

    get_esp_ebp(myesp, myebp);
    print_esp_ebp("Main:  ", myesp, myebp);

    int r = try(&global_ctx, myfunction, 10);

    get_esp_ebp(myesp, myebp);
    print_esp_ebp("Main:  ", myesp, myebp);

    puts("All's good\n");
}
