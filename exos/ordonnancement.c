#include "minilib.h"
#include "list.h"
#include "idt.h"

#define get_esp_ebp(x,y)                            \
    asm("movl %%esp, %0" "\n\t" "movl %%ebp, %1"    \
        : "=r"(x), "=r"(y))

#define put_esp_ebp(x,y)                            \
    asm("movl %0, %%esp" "\n\t" "movl %1, %%ebp"    \
        :                                           \
        : "r"(x), "r"(y))

#define STACK_SIZE 4096

typedef void (*PFUNC)(void *);

enum ctx_state_t {NEW, ACTIVE, TERMINATED, FREE};

struct ctx_s 
{
    void *esp;
    void *ebp;
#define CTX_MAGIC 0xDEADBEEF
    int ctx_magic;

    PFUNC pfunc;
    void *arg;
    enum ctx_state_t state;
    struct list_head mycontext;

    int stack[STACK_SIZE];
};
struct ctx_s ctx_ping; 
struct ctx_s ctx_pong; 
struct ctx_s *current_ctx;
struct ctx_s free_ctx[16];

LIST_HEAD(mylisthead);

void f_ping(void *arg);
void f_pong(void *arg);
void f_pung(void *arg);
void init_ctx(struct ctx_s *ctx, PFUNC f, void *args);
int create_ctx(PFUNC f, void *args);
void sched_handler(idt_register t);
void start_sched();
void yield();
void timer();

void app_main()
{
    for(int i = 0; i < 16; i++) {
        free_ctx[i].ebp = (&free_ctx[i].stack[STACK_SIZE-4]);
        free_ctx[i].esp = (&free_ctx[i].stack[STACK_SIZE-8]);
        free_ctx[i].state = FREE;
        free_ctx[i].ctx_magic = CTX_MAGIC;
    }

    create_ctx(f_ping, NULL);
    create_ctx(f_pong, NULL);
    create_ctx(f_pung, NULL);

    start_sched();
}

void f_ping(void *args)
{
    while(1) {
        putc('A');
timer();
        putc('B');
timer();
        putc('C');
timer();
    }
};

void f_pong(void *args)
{
    while(1) {
        putc('1');
        timer();
        putc('2');
        timer();
        putc('3');
        timer();
    }
};

void f_pung(void *args)
{
    while(1) {
        putc('?');
        timer();
        putc('!');
        timer();
        putc(':');
        timer();
    }
}

int create_ctx(PFUNC pfunc, void *args)
{
    struct ctx_s *context = &free_ctx[0];
    int iterator = 1;
    
    while(context->state != FREE) {
        context = &free_ctx[iterator];
        iterator++;
    }

    context->state = NEW;
    context->arg = args;
    context->pfunc = pfunc;

    putc('\n');
    INIT_LIST_HEAD(&context->mycontext);
    list_add(&context->mycontext, &mylisthead);

    return 0;
}

void sched_handler(idt_register t)
{
   yield();
}

void start_sched() 
{
    idt_setup_handler(0, sched_handler);
}

void timer()
{

    for(int i = 0; i < 10000000; i++);
    yield();
}

void yield()
{
    irq_disable();
    if(current_ctx == NULL) {
        current_ctx = list_first_entry(&mylisthead, struct ctx_s, mycontext);
    } else {
        get_esp_ebp(current_ctx->esp, current_ctx->ebp);
        if(current_ctx == list_last_entry(&mylisthead, struct ctx_s, mycontext)) {
            current_ctx = list_first_entry(&mylisthead, struct ctx_s, mycontext);
        } else {
            current_ctx = list_next_entry(current_ctx, mycontext);
        }
    }

    put_esp_ebp(current_ctx->esp, current_ctx->ebp);

    if(current_ctx->state == NEW) {
        irq_enable();
        end_of_interrupt();
        current_ctx->state = ACTIVE;
        current_ctx->pfunc(current_ctx->arg);
    }
    irq_enable();

    return;
}