#include "minilib.h"
#include "list.h"
#include "idt.h"
#include "context.h"
#include "ioport.h"



struct ctx_s ctx_ping; 
struct ctx_s ctx_pong; 


void f_ping(int_regs_t *t);
void f_pong(int_regs_t *t);
//void init_ctx(struct ctx_s *ctx, PFUNC f, void *args);
//int create_ctx(PFUNC f, void *args);
void sched_handler(int_regs_t *t);
void start_sched();
//void yield();
void timer();

void app_main()
{
    for(int i = 0; i < 16; i++) {
        free_ctx[i].ebp = (&free_ctx[i].stack[STACK_SIZE-4]);
        free_ctx[i].esp = (&free_ctx[i].stack[STACK_SIZE-8]);
        free_ctx[i].state = FREE;
        free_ctx[i].ctx_magic = CTX_MAGIC;
    }

    start_sched();
}

void f_ping(int_regs_t *t)
{
    while(1) {
        putc('A');
timer();
        putc('B');
timer();
        putc('C');
timer();
    }
};

void f_pong(int_regs_t *t)
{
    while(1) {
        putc('1');
        timer();
        putc('2');
        timer();
        putc('3');
        timer();
    }
};


void sched_handler(int_regs_t *t)
{
    yield();
}


void start_sched() 
{
    idt_setup_handler(0, f_ping);
    idt_setup_handler(0, f_pong);
}

void timer()
{
    for(int i = 0; i < 10000000; i++);
}
