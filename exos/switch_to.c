#include "minilib.h"

#define get_esp_ebp(x,y)                            \
    asm("movl %%esp, %0" "\n\t" "movl %%ebp, %1"    \
        : "=r"(x), "=r"(y))

#define put_esp_ebp(x,y)                            \
    asm("movl %0, %%esp" "\n\t" "movl %1, %%ebp"    \
        :                                           \
        : "r"(x), "r"(y))

#define STACK_SIZE 4096

typedef void (*PFUNC)(void *);

enum ctx_state_t {NEW, ACTIVE, TERMINATED};

struct ctx_s ctx_ping; 
struct ctx_s ctx_pong; 
struct ctx_s *current_ctx = NULL;

struct ctx_s 
{
    unsigned int esp;
    unsigned int ebp;
#define CTX_MAGIC 0xDEADBEEF
    int ctx_magic;

    PFUNC pfunc;
    void *arg;
    enum ctx_state_t state;

    int stack[STACK_SIZE];
};

void f_ping(void *arg);
void f_pong(void *arg);
void init_ctx(struct ctx_s *ctx, PFUNC f, void *args);
void switch_to_ctx(struct ctx_s *ctx);


void app_main()
{
    init_ctx(&ctx_ping, f_ping, NULL);
    init_ctx(&ctx_pong, f_pong, NULL);
    switch_to_ctx(&ctx_ping);
};

void init_ctx(struct ctx_s *ctx, PFUNC f, void *args)
{
    ctx->ctx_magic = CTX_MAGIC;
    ctx->pfunc = f;
    ctx->arg = args;
    ctx->state = NEW;
    ctx->ebp = (unsigned int)&ctx->stack[STACK_SIZE - 4];
    ctx->esp = (unsigned int)&ctx->stack[STACK_SIZE - 8];
};

void switch_to_ctx(struct ctx_s *ctx)
{
    if (current_ctx != NULL){
        get_esp_ebp(current_ctx->esp, current_ctx->ebp);
    };

    current_ctx = ctx;
    put_esp_ebp(ctx->esp, ctx->ebp);
    if(current_ctx->state == NEW)
    {
        current_ctx->state = ACTIVE;
        current_ctx->pfunc(current_ctx->arg);
    }
    return;
};

void f_ping(void *args)
{
    while(1) {
        putc('A') ;
        switch_to_ctx(&ctx_pong);
        putc('B') ;
        switch_to_ctx(&ctx_pong);
        putc('C') ;
        switch_to_ctx(&ctx_pong);
    }
};

void f_pong(void *args)
{
    while(1) {
        putc('1') ;
        switch_to_ctx(&ctx_ping);
        putc('2') ;
        switch_to_ctx(&ctx_ping);
    }
};