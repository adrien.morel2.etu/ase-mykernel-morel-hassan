#include "ioport.h"
#include "gdt.h"
#include "idt.h"
#include "context.h"
//#include "ord.h"

void clear_screen();				  /* clear screen */
void putc(char aChar);				/* print a single char on screen */
void puts(char *aString);			/* print a string on the screen */
void puthex(int aNumber);			/* print an Hex number on screen */
void keyboard_irq();
void irq_timer(int_regs_t *r);
void loading(void *args);

unsigned char keyboardEntry[64];



                  
char key_map[] =   {      '0' , '1' , '2' , '3' , '4' , '5' , '6' , '7'  , '8' , '9' , '0'  , '-'  , '=' , '\b' ,
                    '\t' ,'a' , 'z' , 'e' , 'r' , 't' , 'y' , 'u' , 'i' , 'o'  , 'p' , '[' , ']'  , '\n' , 
                    '0' , 'q' , 's'  ,'d' , 'f' , 'g' , 'h' , 'j' , 'k' , 'l' , 'm' , '\'' , '`' , '0' , '\\' , 'w'  , 'x' , 'c' , 'v'  ,
                    'b' , 'n' , ',' , ';' , '.' , '/' , '0' , '*' , '0'  , ' ' , '0' };

char read_keyboard_entry(unsigned char key)
{
  if(--key > 0x3a)
    return '\0';

  return key_map[key];
}

void keyboard_irq() {
  unsigned char in = _inb(0x60);
  if((in & 0x80) == 0) {
    putc(read_keyboard_entry(in));
  }
}




void timer_irq(int_regs_t *r) {
  _outb(0x20, 0x20);
}

/* multiboot entry-point with datastructure as arg. */
void main(unsigned int * mboot_info)
{

    /* clear the screen */
    clear_screen();
    puts("Early boot.\n"); 
    puts("\t-> Setting up the GDT... ");
    gdt_init_default();
    puts("done\n");

    puts("\t-> Setting up the IDT... ");
    setup_idt();
    puts("OK\n");

    puts("\n\n");

    puts("Hello World\n");

    irq_enable();

    //app_main();
    
    irq_enable();
    idt_setup_handler(0, timer_irq);
    idt_setup_handler(1, keyboard_irq);


    //yield();

    for(;;) ; /* nothing more to do... really nothing ! */
}

/*
void loading(void *args)
{
	while (1)
	{
		video_memory[(cursor_x+80*cursor_y)*2]='+';
		video_memory[(cursor_x+80*cursor_y)*2]='*';
    video_memory[(cursor_x+80*cursor_y)*2]=' ';
	}
}*/