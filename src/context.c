#include <minilib.h>
#include "context.h"

#define get_esp_ebp(x,y)                            \
    asm("movl %%esp, %0" "\n\t" "movl %%ebp, %1"    \
        : "=r"(x), "=r"(y))

#define put_esp_ebp(x,y)                            \
    asm("movl %0, %%esp" "\n\t" "movl %1, %%ebp"    \
        :                                           \
        : "r"(x), "r"(y))

typedef void (*PFUNC)(void *);

LIST_HEAD(mylisthead);

void irq_enable() {
     __asm volatile("sti");//set interruption
}

void irq_disable() {
     __asm volatile("cli");//clear interruption
}

void init_ctx(struct ctx_s *ctx, PFUNC f, void *args)
{
    // initialisation du contexte
    ctx->ctx_magic = CTX_MAGIC;
    ctx->pfunc = f;
    ctx->arg = args;
    ctx->state = NEW;
    ctx->ebp = &ctx->stack[STACK_SIZE - 4];
    ctx->esp = &ctx->stack[STACK_SIZE - 8];
};

void switch_to_ctx(struct ctx_s *ctx)
{
    if (current_ctx != NULL){
        get_esp_ebp(current_ctx->esp, current_ctx->ebp);
    };

    current_ctx = ctx;
    put_esp_ebp(ctx->esp, ctx->ebp);
    if(current_ctx->state == NEW)
    {
        current_ctx->state = ACTIVE;
        current_ctx->pfunc(current_ctx->arg);
    }
    return;
};

int create_ctx(PFUNC pfunc, void *args)
{
    struct ctx_s *context = &free_ctx[0];
    int iterator = 1;
    
    while(context->state != FREE) {
        context = &free_ctx[iterator];
        iterator++;
    }

    context->state = NEW;
    context->arg = args;
    context->pfunc = pfunc;

    putc('\n');
    INIT_LIST_HEAD(&context->mycontext);
    list_add(&context->mycontext, &mylisthead);

    return 0;
}

void yield()
{
    if(current_ctx == NULL) {
        current_ctx = list_first_entry(&mylisthead, struct ctx_s, mycontext);
    } else {
        get_esp_ebp(current_ctx->esp, current_ctx->ebp);
        if(current_ctx == list_last_entry(&mylisthead, struct ctx_s, mycontext)) {
            current_ctx = list_first_entry(&mylisthead, struct ctx_s, mycontext);
        } else {
            current_ctx = list_next_entry(current_ctx, mycontext);
        }
    }

    put_esp_ebp(current_ctx->esp, current_ctx->ebp);

    if(current_ctx->state == NEW) {
        current_ctx->state = ACTIVE;
        current_ctx->pfunc(current_ctx->arg);
    }

    return;
}